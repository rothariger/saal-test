﻿using MediatR;
using Saal.DTO;

namespace Saal.Queries.UserQueries
{
    public class GetAllUsersQuery : IRequest<List<User>>
    {
    }
}
