﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;
using Saal.DTO;

namespace Saal.Queries.UserQueries
{
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, List<User>>
    {
        private readonly SaalContext _Context;

        public GetAllUsersQueryHandler(SaalContext Context)
        {
            _Context = Context;
        }

        public async Task<List<User>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            return await _Context.Users.Include(r => r.Roles).ToListAsync();
        }
    }
}
