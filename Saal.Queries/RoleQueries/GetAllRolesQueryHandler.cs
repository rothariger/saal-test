﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;
using Saal.DTO;

namespace Saal.Queries.RoleQueries
{
    public class GetAllRolesQueryHandler : IRequestHandler<GetAllRolesQuery, List<Role>>
    {
        private readonly SaalContext _Context;

        public GetAllRolesQueryHandler(SaalContext Context)
        {
            _Context = Context;
        }

        public async Task<List<Role>> Handle(GetAllRolesQuery request, CancellationToken cancellationToken)
        {
            return await _Context.Roles.ToListAsync();
        }
    }
}
