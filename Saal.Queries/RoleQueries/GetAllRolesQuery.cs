﻿using MediatR;
using Saal.DTO;

namespace Saal.Queries.RoleQueries
{
    public class GetAllRolesQuery : IRequest<List<Role>>
    {
    }
}
