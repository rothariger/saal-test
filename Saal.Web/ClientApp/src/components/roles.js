import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
    Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton,
    Typography
} from '@mui/material';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Toolbar from '@mui/material/Toolbar';
import Tooltip from '@mui/material/Tooltip';
import { DataGrid } from '@mui/x-data-grid';
import React, { Component } from 'react';

export class Roles extends Component {
    static displayName = Roles.name;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            modalCreate: false,
            loading: true,
            ModalData: {
                name: ''
            },
            rolesSelection: [],
            confirmDialog: false,
            filterText: ''
        };
    }

    componentDidMount() {
        this.populateRoles();
    }

    renderGrid() {
        const columns = [
            { field: 'roleId', headerName: 'ID', width: 70 },
            { field: 'name', headerName: 'Role Name', width: 400 },
        ];

        const setSelection = (ids) => {
            this.setState({ rolesSelection: ids });
        };

        const filteredRows = () => {
            const { data, filterText } = this.state;

            if (filterText) {
                return data.filter(f => f.name.toLowerCase().includes(filterText));
            }
            return data;
        };

        return (
            <div style={{ height: 500, width: '100%' }}>
                <DataGrid
                    rows={filteredRows()}
                    columns={columns}
                    rowsPerPageOptions={[5, 10, 25, 100]}
                    checkboxSelection
                    getRowId={(row) => row.roleId}
                    onSelectionModelChange={(ids) => setSelection(ids)}
                />
            </div>);
    }

    renderToolbar() {
        const numSelected = this.state.rolesSelection.length;
        const handleFilterInputChange = event => {
            let { filterText } = this.state;
            filterText = event.target.value;
            this.setState({ filterText: filterText })
        };

        return (<Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 }
            }}>
            <Typography
                sx={{ flex: '1 1 100%' }}
                color="inherit"
                variant="subtitle1"
                component="div"
            >
                <TextField
                    margin="dense"
                    variant="outlined"
                    label="Filter"
                    defaultValue=""
                    id="filter"
                    value={this.state.filterText}
                    onChange={handleFilterInputChange}
                >
                </TextField>
            </Typography>
            <Tooltip title="Add">
                <IconButton onClick={() => { this.createRole(); }}>
                    <AddIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Edit">
                <IconButton disabled={numSelected != 1} onClick={() => { this.editRole(); }}>
                    <EditIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
                <IconButton disabled={numSelected == 0} onClick={() => { this.deleteRole(); }}>
                    <DeleteIcon />
                </IconButton>
            </Tooltip>
        </Toolbar>);
    }

    renderConfirmDialog() {
        const closeConfirmDialog = () => {
            this.setState({ confirmDialog: false });
        };

        return (<Dialog open={this.state.confirmDialog} maxWidth="sm" fullWidth>
            <DialogTitle>Confirm?</DialogTitle>
            <Box position="absolute" top={0} right={0}>
                <IconButton onClick={() => { closeConfirmDialog(); }} >
                    <CloseIcon />
                </IconButton>
            </Box>
            <DialogContent>
                <Typography>Do you want to delete the selected record(s)?</Typography>
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant="contained" onClick={() => { closeConfirmDialog(); }}>
                    Cancel
                </Button>
                <Button color="secondary" variant="contained" onClick={() => { this.confirmDeleteRole(); }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>);
    }

    renderDialog() {
        let modalCreate = this.state.modalCreate;
        const handleTextInputChange = event => {
            let ModalData = { ...this.state.ModalData }
            ModalData.name = event.target.value;
            this.setState({ ModalData })
        };

        return (<Dialog
            fullWidth
            maxWidth="md"
            open={modalCreate}
            onClose={() => this.handleClose()}
        >
            <DialogContent >
                <Grid container>
                    <Grid item xs={8}>
                        <Grid container direction="row" >
                            <Grid item xs={8}>
                                <Typography variant="h6">
                                    {this.state.ModalData.roleId > 0 ? 'Edit Role' : 'Add new Role'}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction="row"
                            spacing={1}
                        >
                            <Grid item xs={10}>
                                <TextField
                                    style={{ marginBottom: 20 }}
                                    fullWidth
                                    margin="dense"
                                    variant="outlined"
                                    label="Role Name"
                                    defaultValue=""
                                    id="shipping-presets"
                                    value={this.state.ModalData.name}
                                    onChange={handleTextInputChange}
                                >
                                </TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant="contained" onClick={() => { this.handleClose(); }}>
                    Cancel
                </Button>
                <Button color="secondary" variant="contained" onClick={() => { this.saveRole(); }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p> :
            this.renderGrid();

        return (
            <div>
                <h1 id="tabelLabel" >Roles</h1>
                {this.renderToolbar()}
                {contents}


                {this.renderDialog()}
                {this.renderConfirmDialog()}
            </div>
        );
    }

    createRole() {
        let data = { ...this.state.ModalData };
        data.name = '';
        data.roleId = null;
        this.setState({ ModalData: data });
        this.setState({ modalCreate: true });
    }

    editRole() {
        const id = this.state.rolesSelection[0];
        const row = this.state.data.find(f => f.roleId == id);
        let data = { ...row };
        this.setState({ ModalData: data });
        this.setState({ modalCreate: true });

    }

    async deleteRole() {
        this.setState({ confirmDialog: true });
    }

    handleClose = () => this.setState({ modalCreate: false });

    async saveRole() {
        if (!this.state.ModalData.name)
            return;
        this.setState({ modalCreate: false });

        var result;
        let data = this.state.ModalData;
        if (data.roleId > 0) {
            const requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            };
            result = await fetch('api/role', requestOptions);
        }
        else {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };
            result = await fetch('api/role?Name=' + data.name, requestOptions);
        }
        const body = await result.json();
        if (body.roleId > 0 || body === true) {
            await this.populateRoles();
        }
        else {
            alert('The role was not created, contact administration.');
        }
    }

    async confirmDeleteRole() {
        if (this.state.rolesSelection.length > 0) {
            let data = this.state.rolesSelection.join(';');
            if (data.length > 0) {
                const requestOptions = {
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json' },
                };
                let response = await fetch('api/role?Ids=' + data, requestOptions);
                let result = await response.json();
                if (result === true) {
                    await this.populateRoles();
                    this.setState({ confirmDialog: false });
                }
                else {
                    alert('The role was not deleted, contact administration.');
                }
            }
        }

    }

    async populateRoles() {
        const populateData = await fetch('api/role');
        const data = await populateData.json();
        this.setState({ data: data, loading: false });
    }


}
