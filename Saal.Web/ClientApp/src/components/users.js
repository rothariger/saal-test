import AddIcon from '@mui/icons-material/Add';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
    Autocomplete, Box, Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, Grid, IconButton, TextField,
    Toolbar, Tooltip, Typography
} from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import React, { Component } from 'react';

export class Users extends Component {
    static displayName = Users.name;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            roles: [],
            modalCreate: false,
            loading: true,
            ModalData: {
                name: '',
                roles: []
            },
            usersSelection: [],
            confirmDialog: false,
            filterText: ''
        };
    }

    componentDidMount() {
        this.populateUsers();
        this.populateRoles();
    }

    renderGrid() {
        const columns = [
            { field: 'userId', headerName: 'ID', width: 70 },
            { field: 'username', headerName: 'User Name', width: 300 },
            { field: 'userRoles', headerName: 'User Roles', width: 300 },
        ];

        const setSelection = (ids) => {
            this.setState({ usersSelection: ids });
        };

        const filteredRows = () => {
            const { data, filterText }  = this.state;

            if (filterText) {
                return data.filter(f => f.username.toLowerCase().includes(filterText));
            }
            return data;
        };

        return (
            <div style={{ height: 500, width: '100%' }}>
                <DataGrid
                    rows={filteredRows()}
                    columns={columns}
                    rowsPerPageOptions={[5, 10, 25, 100]}
                    checkboxSelection
                    getRowId={(row) => row.userId}
                    onSelectionModelChange={(ids) => setSelection(ids)}
                />
            </div>);
    }

    renderToolbar() {
        const numSelected = this.state.usersSelection.length;
        const handleFilterInputChange = event => {
            let { filterText } = this.state;
            filterText = event.target.value;
            this.setState({ filterText: filterText })
        };

        return (<Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 }
            }}>
            <Typography
                sx={{ flex: '1 1 100%' }}
                color="inherit"
                variant="subtitle1"
                component="div"
            >
                <TextField
                    margin="dense"
                    variant="outlined"
                    label="Filter"
                    defaultValue=""
                    id="filter"
                    value={this.state.filterText}
                    onChange={handleFilterInputChange}
                >
                </TextField>
            </Typography>
            <Tooltip title="Add">
                <IconButton onClick={() => { this.createUser(); }}>
                    <AddIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Edit">
                <IconButton disabled={numSelected != 1} onClick={() => { this.editUser(); }}>
                    <EditIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
                <IconButton disabled={numSelected == 0} onClick={() => { this.deleteUser(); }}>
                    <DeleteIcon />
                </IconButton>
            </Tooltip>
        </Toolbar>);
    }

    renderConfirmDialog() {
        const closeConfirmDialog = () => {
            this.setState({ confirmDialog: false });
        };

        return (<Dialog open={this.state.confirmDialog} maxWidth="sm" fullWidth>
            <DialogTitle>Confirm?</DialogTitle>
            <Box position="absolute" top={0} right={0}>
                <IconButton onClick={() => { closeConfirmDialog(); }} >
                    <CloseIcon />
                </IconButton>
            </Box>
            <DialogContent>
                <Typography>Do you want to delete the selected record(s)?</Typography>
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant="contained" onClick={() => { closeConfirmDialog(); }}>
                    Cancel
                </Button>
                <Button color="secondary" variant="contained" onClick={() => { this.confirmDeleteUser(); }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>);
    }

    renderDialog() {
        let modalCreate = this.state.modalCreate;
        let arSelected = this.state.ModalData.roles.map(r => this.state.roles.find(rs => rs.roleId == r.roleId));
        let selectedRoles = arSelected;
        const handleTextInputChange = event => {
            let ModalData = { ...this.state.ModalData }
            ModalData.username = event.target.value;
            this.setState({ ModalData })
        };

        const setModelRole = value => {
            let ModalData = { ...this.state.ModalData }
            ModalData.roles = value;
            this.setState({ ModalData })
        };
        const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
        const checkedIcon = <CheckBoxIcon fontSize="small" />;

        return (<Dialog
            fullWidth
            maxWidth="md"
            open={modalCreate}
            onClose={() => this.handleClose()} >
            <DialogContent >
                <Grid container>
                    <Grid item xs={8}>
                        <Grid container direction="row" >
                            <Grid item xs={8}>
                                <Typography variant="h6">
                                    {this.state.ModalData.userId > 0 ? 'Edit User' : 'Add new User'}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction="row"
                            spacing={1}
                        >
                            <Grid item xs={10}>
                                <TextField
                                    style={{ marginBottom: 20 }}
                                    fullWidth
                                    margin="dense"
                                    variant="outlined"
                                    label="Name"
                                    defaultValue=""
                                    id="username"
                                    value={this.state.ModalData.username}
                                    onChange={handleTextInputChange}
                                >
                                </TextField>
                            </Grid>
                            <Grid item xs={10}>
                                <Autocomplete
                                    multiple
                                    id="autoRoles"
                                    options={this.state.roles}
                                    disableCloseOnSelect
                                    getOptionLabel={(option) => option.name}
                                    renderOption={(props, option, { selected }) => (
                                        <li {...props}>
                                            <Checkbox
                                                icon={icon}
                                                checkedIcon={checkedIcon}
                                                style={{ marginRight: 8 }}
                                                checked={selected}
                                            />
                                            {option.name}
                                        </li>
                                    )}
                                    style={{ marginBottom: 20 }}
                                    onChange={(event, newValue) => {
                                        setModelRole(newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField {...params} label="Roles" />
                                    )}
                                    value={selectedRoles}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant="contained" onClick={() => { this.handleClose(); }}>
                    Cancel
                </Button>
                <Button color="secondary" variant="contained" onClick={() => { this.saveUser(); }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p> :
            this.renderGrid();

        return (
            <div>
                <h1 id="tabelLabel">Users</h1>
                {this.renderToolbar()}
                {contents}

                {this.renderConfirmDialog()}

                {this.renderDialog()}
            </div>
        );
    }

    createUser() {
        let data = { ...this.state.ModalData };
        data.name = '';
        data.userId = null;
        this.setState({ ModalData: data });
        this.setState({ modalCreate: true });
    }

    editUser() {
        const id = this.state.usersSelection[0];
        const row = this.state.data.find(f => f.userId == id);
        let data = { ...row };
        this.setState({ ModalData: data });
        this.setState({ modalCreate: true });
    }

    async deleteUser() {
        this.setState({ confirmDialog: true });
    }

    handleClose = () => this.setState({ modalCreate: false });

    async saveUser() {
        if (!this.state.ModalData.username)
            return;
        this.setState({ modalCreate: false });

        var result;
        let data = this.state.ModalData;
        if (data.userId > 0) {
            const requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            };
            result = await fetch('api/user', requestOptions);
        }
        else {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' }
            };
            let roles = data.roles.map(r => r.roleId).join(';');
            result = await fetch(`api/user?Name=${data.username}&Roles=${roles}`, requestOptions);
        }
        const body = await result.json();
        if (body.userId > 0 || body === true) {
            await this.populateUsers();
        }
        else {
            alert('The User was not created, contact administration.');
        }
    }

    async confirmDeleteUser() {
        if (this.state.usersSelection.length > 0) {
            let data = this.state.usersSelection.join(';');
            if (data.length > 0) {
                const requestOptions = {
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json' },
                };
                let response = await fetch('api/user?Ids=' + data, requestOptions);
                let result = await response.json();
                if (result === true) {
                    await this.populateUsers();
                    this.setState({ confirmDialog: false });
                }
                else {
                    alert('The user was not deleted, contact administration.');
                }
            }
        }
    }

    async populateUsers() {
        const populateData = await fetch('api/user');
        var data = await populateData.json();
        data = data.map(r => {
            r.userRoles = r.roles.map(rs => rs.name).join(';');
            return r;
        });
        this.setState({ data: data, loading: false });
    }

    async populateRoles() {
        const populateData = await fetch('api/role');
        const data = await populateData.json();
        this.setState({ roles: data });
    }
}
