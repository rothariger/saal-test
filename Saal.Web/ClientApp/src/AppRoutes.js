import { Users } from "./components/users";
import { Roles } from "./components/roles";

const AppRoutes = [
    {
        index: true,
        path: '/users',
        element: <Users />
    },
    {
        path: '/roles',
        element: <Roles />
    }
];

export default AppRoutes;
