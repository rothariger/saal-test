﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Saal.DTO;
using Saal.Services;
using Saal.Services.Interfaces;

namespace Saal.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserServices _UserServices;
        public UserController(IUserServices UserServices)
        {
            _UserServices = UserServices;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> List()
        {
            return await _UserServices.List();
        }

        [HttpPost]
        public async Task<User> Insert(string Name, string Roles)
        {
            return await _UserServices.Insert(Name, Roles);
        }

        [HttpPut]
        public async Task<bool> Update([FromBody] User User)
        {
            return await _UserServices.Update(User);
        }

        [HttpDelete]
        public async Task<bool> Delete(string ids)
        {
            return await _UserServices.Delete(ids);
        }
    }
}
