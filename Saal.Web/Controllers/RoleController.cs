﻿using Microsoft.AspNetCore.Mvc;
using Saal.DTO;
using Saal.Services.Interfaces;

namespace Saal.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleServices _RoleServices;
        public RoleController(IRoleServices RoleServices)
        {
            _RoleServices = RoleServices;
        }

        [HttpGet]
        public async Task<IEnumerable<Role>> List()
        {
            return await _RoleServices.List();
        }

        [HttpPost]
        public async Task<Role> Insert(string Name)
        {
            return await _RoleServices.Insert(Name);
        }

        [HttpPut]
        public async Task<bool> Update([FromBody] Role Role)
        {
            return await _RoleServices.Update(Role);
        }

        [HttpDelete]
        public async Task<bool> Delete(string ids)
        {
            return await _RoleServices.Delete(ids);
        }
    }
}
