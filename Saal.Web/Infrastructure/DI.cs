﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Commands.RoleCommands;
using Saal.Commands.UserCommands;
using Saal.Dal;
using Saal.DTO;
using Saal.Queries.RoleQueries;
using Saal.Queries.UserQueries;
using Saal.Services;
using Saal.Services.Interfaces;

namespace Saal.Web.Infrastructure
{
    public static class DI
    {
        public static void InitializeInjections(this IServiceCollection services)
        {
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<SaalContext>(options =>
            {
                options.UseSqlServer("name=ConnectionStrings:SaalTest");
            });

            // Services
            services.AddTransient<IUserServices, UserServices>();
            services.AddTransient<IRoleServices, RoleServices>();
        }

        public static IServiceCollection AddCommandHandlers(this IServiceCollection services)
        {
            services.AddScoped<IRequestHandler<CreateUserCommand, User>, CreateUserCommandHandler>();
            services.AddScoped<IRequestHandler<DeleteUserCommand, bool>, DeleteUserCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateUserCommand, bool>, UpdateUserCommandHandler>();
            services.AddScoped<IRequestHandler<GetAllUsersQuery, List<User>>, GetAllUsersQueryHandler>();

            services.AddScoped<IRequestHandler<CreateRoleCommand, Role>, CreateRoleCommandHandler>();
            services.AddScoped<IRequestHandler<DeleteRoleCommand, bool>, DeleteRoleCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateRoleCommand, bool>, UpdateRoleCommandHandler>();
            services.AddScoped<IRequestHandler<GetAllRolesQuery, List<Role>>, GetAllRolesQueryHandler>();

            return services;
        }
    }
}
