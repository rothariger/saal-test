﻿using MediatR;
using Saal.Dal;
using Saal.DTO;

namespace Saal.Commands.RoleCommands
{
    public class CreateRoleCommandHandler : IRequestHandler<CreateRoleCommand, Role>
    {
        private readonly SaalContext _dbContext;

        public CreateRoleCommandHandler(SaalContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Role> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            var role = new Role()
            {
                Name = request.Name,
            };
            await _dbContext.Roles.AddAsync(role);
            await _dbContext.SaveChangesAsync();
            return role;
        }
    }
}
