﻿using MediatR;
using Saal.DTO;

namespace Saal.Commands.RoleCommands
{
    public class CreateRoleCommand : IRequest<Role>
    {
        public string Name { get; set; }
    }
}
