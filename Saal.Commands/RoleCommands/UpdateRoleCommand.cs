﻿using MediatR;
using Saal.DTO;

namespace Saal.Commands.RoleCommands
{
    public class UpdateRoleCommand : IRequest<bool>
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
