﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;

namespace Saal.Commands.RoleCommands
{
    public class UpdateRoleCommandHandler : IRequestHandler<UpdateRoleCommand, bool>
    {
        private readonly SaalContext _dbContext;

        public UpdateRoleCommandHandler(SaalContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(UpdateRoleCommand request, CancellationToken cancellationToken)
        {
            var dbrole = await _dbContext.Roles.FirstOrDefaultAsync(u => u.RoleId.Equals(request.RoleId));
            if (dbrole != null)
            {
                dbrole.Name = request.Name;
                dbrole.Users = request.Users;
                await _dbContext.SaveChangesAsync();
                return true;
            }
            else
            {
                throw new ArgumentException("Role not found");
            }
        }
    }
}
