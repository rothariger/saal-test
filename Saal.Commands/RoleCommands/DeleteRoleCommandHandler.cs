﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;

namespace Saal.Commands.RoleCommands
{
    public class DeleteRoleCommandHandler : IRequestHandler<DeleteRoleCommand, bool>
    {
        private readonly SaalContext _dbContext;

        public DeleteRoleCommandHandler(SaalContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(DeleteRoleCommand request, CancellationToken cancellationToken)
        {
            var role = await _dbContext.Roles.FirstOrDefaultAsync(u => u.RoleId.Equals(request.RoleId));
            if (role != null)
            {
                _dbContext.Roles.Remove(role);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            else
            {
                throw new ArgumentException("Role not found");
            }
        }
    }
}
