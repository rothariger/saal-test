﻿using MediatR;

namespace Saal.Commands.RoleCommands
{
    public class DeleteRoleCommand : IRequest<bool>
    {
        public int RoleId { get; set; }
    }
}
