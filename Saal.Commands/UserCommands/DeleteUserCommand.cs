﻿using MediatR;

namespace Saal.Commands.UserCommands
{
    public class DeleteUserCommand : IRequest<bool>
    {
        public int UserId { get; set; }
    }
}
