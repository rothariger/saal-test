﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;
using Saal.DTO;

namespace Saal.Commands.UserCommands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly SaalContext _dbContext;

        public UpdateUserCommandHandler(SaalContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var dbuser = await _dbContext.Users.Include(u => u.Roles).FirstOrDefaultAsync(u => u.UserId.Equals(request.UserId));
            if (dbuser != null)
            {
                dbuser.Username = request.Name;
                _dbContext.Entry(dbuser).State = EntityState.Modified;
                dbuser.Roles.Clear();
                foreach (var role in request.Roles)
                    dbuser.Roles.Add(_dbContext.Roles.First(r => r.RoleId.Equals(role.RoleId)));
                await _dbContext.SaveChangesAsync();
                return true;
            }
            else
            {
                throw new ArgumentException("User not found");
            }
        }
    }
}
