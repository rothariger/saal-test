﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Saal.Dal;
using Saal.DTO;

namespace Saal.Commands.UserCommands
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, User>
    {
        private readonly SaalContext _dbContext;

        public CreateUserCommandHandler(SaalContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = new User()
            {
                Username = request.Name,
                Roles = request.Roles
            };
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
            return user;
        }
    }
}
