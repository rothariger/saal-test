﻿using MediatR;
using Saal.DTO;

namespace Saal.Commands.UserCommands
{
    public class CreateUserCommand : IRequest<User>
    {
        public string Name { get; set; }
        public List<Role> Roles { get; set; }
    }
}
