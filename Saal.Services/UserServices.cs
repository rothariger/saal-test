﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Saal.Commands.UserCommands;
using Saal.DTO;
using Saal.Queries.RoleQueries;
using Saal.Queries.UserQueries;
using Saal.Services.Interfaces;

namespace Saal.Services
{
    public class UserServices : IUserServices
    {
        private readonly IRequestHandler<GetAllUsersQuery, List<User>> _GetAllUsersQuery;
        private readonly IRequestHandler<GetAllRolesQuery, List<Role>> _GetAllRolesQuery;
        private readonly IRequestHandler<CreateUserCommand, User> _CreateUserCommand;
        private readonly IRequestHandler<UpdateUserCommand, bool> _UpdateUserCommand;
        private readonly IRequestHandler<DeleteUserCommand, bool> _DeleteUserCommand;

        private readonly ILogger<UserServices> _Logger;

        public UserServices(ILogger<UserServices> Logger,
            IRequestHandler<GetAllUsersQuery, List<User>> GetAllUsersQuery,
            IRequestHandler<GetAllRolesQuery, List<Role>> GetAllRolesQuery,
            IRequestHandler<CreateUserCommand, User> CreateUserCommand,
            IRequestHandler<UpdateUserCommand, bool> UpdateUserCommand,
            IRequestHandler<DeleteUserCommand, bool> DeleteUserCommand)
        {
            _Logger = Logger;
            _GetAllUsersQuery = GetAllUsersQuery;
            _GetAllRolesQuery = GetAllRolesQuery;
            _CreateUserCommand = CreateUserCommand;
            _UpdateUserCommand = UpdateUserCommand;
            _DeleteUserCommand = DeleteUserCommand;
        }

        public async Task<bool> Delete(string ids)
        {
            try
            {
                foreach (int id in ids.Split(';', StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)))
                {
                    await _DeleteUserCommand.Handle(new DeleteUserCommand() { UserId = id }, CancellationToken.None);
                }
                return true;
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to delete the user.", ex);
            }
            return false;
        }

        public async Task<User> Insert(string Name, string Roles)
        {
            try
            {
                var user = new CreateUserCommand()
                {
                    Name = Name
                };

                if (Roles.Trim().Length > 0)
                {
                    var rList = await _GetAllRolesQuery.Handle(new GetAllRolesQuery(), CancellationToken.None);
                    var arrayRoles = Roles.Trim().Split(";", StringSplitOptions.RemoveEmptyEntries);
                    user.Roles = rList.Where(r => arrayRoles.Contains(r.RoleId.ToString())).ToList();
                }

                return await _CreateUserCommand.Handle(user, System.Threading.CancellationToken.None);
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to insert the user.", ex);
            }
            return null;
        }

        public async Task<IList<User>> List()
        {
            return await _GetAllUsersQuery.Handle(new GetAllUsersQuery(), CancellationToken.None);
        }

        public async Task<bool> Update(User user)
        {
            try
            {
                var cmdUser = new UpdateUserCommand()
                {
                    UserId = user.UserId,
                    Name = user.Username,
                    Roles = user.Roles.ToList()
                };
                return await _UpdateUserCommand.Handle(cmdUser, CancellationToken.None);
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to update the user.", ex);
            }
            return false;
        }
    }
}
