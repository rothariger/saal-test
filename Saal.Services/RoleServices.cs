﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Saal.Commands.RoleCommands;
using Saal.DTO;
using Saal.Queries.RoleQueries;
using Saal.Services.Interfaces;

namespace Saal.Services
{
    public class RoleServices : IRoleServices
    {
        private readonly ILogger<RoleServices> _Logger;
        private readonly IRequestHandler<GetAllRolesQuery, List<Role>> _GetAllRolesQuery;
        private readonly IRequestHandler<CreateRoleCommand, Role> _CreateRoleCommand;
        private readonly IRequestHandler<UpdateRoleCommand, bool> _UpdateRoleCommand;
        private readonly IRequestHandler<DeleteRoleCommand, bool> _DeleteRoleCommand;

        public RoleServices(ILogger<RoleServices> Logger,
                            IRequestHandler<GetAllRolesQuery, List<Role>> GetAllRolesQuery,
                            IRequestHandler<CreateRoleCommand, Role> CreateRoleCommand,
                            IRequestHandler<UpdateRoleCommand, bool> UpdateRoleCommand,
                            IRequestHandler<DeleteRoleCommand, bool> DeleteRoleCommand
)
        {
            _Logger = Logger;
            _GetAllRolesQuery = GetAllRolesQuery;
            _CreateRoleCommand = CreateRoleCommand;
            _UpdateRoleCommand = UpdateRoleCommand;
            _DeleteRoleCommand = DeleteRoleCommand;
        }

        public async Task<bool> Delete(string ids)
        {
            try
            {
                foreach (int id in ids.Split(';', StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)))
                {
                    await _DeleteRoleCommand.Handle(new DeleteRoleCommand() { RoleId = id }, CancellationToken.None);
                }
                return true;
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to delete the role.", ex);
            }
            return false;
        }

        public async Task<Role> Insert(string Name)
        {
            try
            {
                var role = new CreateRoleCommand()
                {
                    Name = Name
                };

                return await _CreateRoleCommand.Handle(role, CancellationToken.None);
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to insert the role.", ex);
            }
            return null;
        }

        public async Task<IList<Role>> List()
        {
            return await _GetAllRolesQuery.Handle(new GetAllRolesQuery(), CancellationToken.None);
        }

        public async Task<bool> Update(Role role)
        {
            try
            {
                var updateRole = new UpdateRoleCommand()
                {
                    RoleId = role.RoleId,
                    Name = role.Name,
                    Users = role.Users.ToList()
                };
                await _UpdateRoleCommand.Handle(updateRole, CancellationToken.None);

                return true;
            }
            catch (Exception ex)
            {
                _Logger.LogError("Exception occurred while trying to update the role.", ex);
            }
            return false;
        }
    }
}
