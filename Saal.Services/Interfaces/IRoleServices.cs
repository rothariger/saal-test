﻿using Saal.DTO;

namespace Saal.Services.Interfaces
{
    public interface IRoleServices
    {
        Task<IList<Role>> List();
        Task<Role> Insert(string Name);
        Task<bool> Update(Role role);
        Task<bool> Delete(string ids);
    }
}
