﻿using Saal.DTO;

namespace Saal.Services.Interfaces
{
    public interface IUserServices
    {
        Task<IList<User>> List();
        Task<User> Insert(string Name, string Roles);
        Task<bool> Update(User user);
        Task<bool> Delete(string ids);
    }
}
