global using NUnit.Framework;
global using MediatR;
global using Microsoft.Extensions.Logging;
global using Moq;
global using Saal.DTO;
global using Saal.Services.Interfaces;
