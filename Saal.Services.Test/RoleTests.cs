using Saal.Commands.RoleCommands;
using Saal.Queries.RoleQueries;

namespace Saal.Services.Test
{
    [TestFixture]
    public class RoleTests
    {
        private Mock<ILogger<RoleServices>> _Logger;
        private Mock<IRequestHandler<CreateRoleCommand, Role>> _createRoleHandlerMock;
        private Mock<IRequestHandler<UpdateRoleCommand, bool>> _updateRoleHandlerMock;
        private Mock<IRequestHandler<DeleteRoleCommand, bool>> _deleteRoleHandlerMock;
        private Mock<IRequestHandler<GetAllRolesQuery, List<Role>>> _getAllRolesHandlerMock;
        private IRoleServices _roleServices;

        [SetUp]
        public void Setup()
        {
            _Logger = new Mock<ILogger<RoleServices>>();
            _createRoleHandlerMock = new Mock<IRequestHandler<CreateRoleCommand, Role>>();
            _updateRoleHandlerMock = new Mock<IRequestHandler<UpdateRoleCommand, bool>>();
            _deleteRoleHandlerMock = new Mock<IRequestHandler<DeleteRoleCommand, bool>>();
            _getAllRolesHandlerMock = new Mock<IRequestHandler<GetAllRolesQuery, List<Role>>>();

            _roleServices = new RoleServices(
                _Logger.Object,
                _getAllRolesHandlerMock.Object,
                _createRoleHandlerMock.Object,
                _updateRoleHandlerMock.Object,
                _deleteRoleHandlerMock.Object
                );
        }

        [Test]
        public async Task CallCreateRoleCommandHandler()
        {
            // Arrange
            var expectedRole = new Role() { Name = "Role 1" };
            _createRoleHandlerMock.Setup(handler => handler.Handle(It.IsAny<CreateRoleCommand>(), CancellationToken.None)).ReturnsAsync(expectedRole);

            // Act
            var result = await _roleServices.Insert("Role 1");

            // Assert
            _createRoleHandlerMock.Verify(handler => handler.Handle(It.IsAny<CreateRoleCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(expectedRole, result);
        }

        [Test]
        public async Task CallUpdateRoleCommandHandler()
        {
            // Arrange
            var role = new Role() { Name = "Role 1" };
            _updateRoleHandlerMock.Setup(handler => handler.Handle(It.IsAny<UpdateRoleCommand>(), CancellationToken.None)).ReturnsAsync(true);

            // Act
            var result = await _roleServices.Update(role);

            // Assert
            _updateRoleHandlerMock.Verify(handler => handler.Handle(It.IsAny<UpdateRoleCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task CallDeleteRoleCommandHandler()
        {
            // Arrange
            _deleteRoleHandlerMock.Setup(handler => handler.Handle(It.IsAny<DeleteRoleCommand>(), CancellationToken.None)).ReturnsAsync(true);

            // Act
            var result = await _roleServices.Delete("1");

            // Assert
            _deleteRoleHandlerMock.Verify(handler => handler.Handle(It.IsAny<DeleteRoleCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task CallGetAllRolesQueryHandler()
        {
            // Arrange
            var expectedRoles = new List<Role>() { new Role() { Name = "Role 1" }, new Role() { Name = "Role 2" } };
            _getAllRolesHandlerMock.Setup(handler => handler.Handle(It.IsAny<GetAllRolesQuery>(), CancellationToken.None)).ReturnsAsync(expectedRoles);

            // Act
            var result = await _roleServices.List();

            // Assert
            _getAllRolesHandlerMock.Verify(handler => handler.Handle(It.IsAny<GetAllRolesQuery>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(expectedRoles, result);
        }
    }
}