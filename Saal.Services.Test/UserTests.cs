using Saal.Commands.UserCommands;
using Saal.Queries.RoleQueries;
using Saal.Queries.UserQueries;

namespace Saal.Services.Test
{
    [TestFixture]
    public class UserTest
    {
        private Mock<ILogger<UserServices>> _Logger;
        private Mock<IRequestHandler<CreateUserCommand, User>> _createUserHandlerMock;
        private Mock<IRequestHandler<UpdateUserCommand, bool>> _updateUserHandlerMock;
        private Mock<IRequestHandler<DeleteUserCommand, bool>> _deleteUserHandlerMock;
        private Mock<IRequestHandler<GetAllUsersQuery, List<User>>> _getAllUsersHandlerMock;
        private Mock<IRequestHandler<GetAllRolesQuery, List<Role>>> _getAllRolesHandlerMock;
        private IUserServices _userServices;

        [SetUp]
        public void Setup()
        {
            _Logger = new Mock<ILogger<UserServices>>();
            _createUserHandlerMock = new Mock<IRequestHandler<CreateUserCommand, User>>();
            _updateUserHandlerMock = new Mock<IRequestHandler<UpdateUserCommand, bool>>();
            _deleteUserHandlerMock = new Mock<IRequestHandler<DeleteUserCommand, bool>>();
            _getAllUsersHandlerMock = new Mock<IRequestHandler<GetAllUsersQuery, List<User>>>();
            _getAllRolesHandlerMock = new Mock<IRequestHandler<GetAllRolesQuery, List<Role>>>();

            _userServices = new UserServices(
                _Logger.Object,
                _getAllUsersHandlerMock.Object,
                _getAllRolesHandlerMock.Object,
                _createUserHandlerMock.Object,
                _updateUserHandlerMock.Object,
                _deleteUserHandlerMock.Object
                );
        }

        [Test]
        public async Task CallCreateUserCommandHandler()
        {
            // Arrange
            var expectedUser = new User() { Username = "User 1" };
            _createUserHandlerMock.Setup(handler => handler.Handle(It.IsAny<CreateUserCommand>(), CancellationToken.None)).ReturnsAsync(expectedUser);

            // Act
            var result = await _userServices.Insert("User 1", string.Empty);

            // Assert
            _createUserHandlerMock.Verify(handler => handler.Handle(It.IsAny<CreateUserCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(expectedUser, result);
        }

        [Test]
        public async Task CallUpdateUserCommandHandler()
        {
            // Arrange
            var User = new User() { Username = "User 1" };
            _updateUserHandlerMock.Setup(handler => handler.Handle(It.IsAny<UpdateUserCommand>(), CancellationToken.None)).ReturnsAsync(true);

            // Act
            var result = await _userServices.Update(User);

            // Assert
            _updateUserHandlerMock.Verify(handler => handler.Handle(It.IsAny<UpdateUserCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task CallDeleteUserCommandHandler()
        {
            // Arrange
            _deleteUserHandlerMock.Setup(handler => handler.Handle(It.IsAny<DeleteUserCommand>(), CancellationToken.None)).ReturnsAsync(true);

            // Act
            var result = await _userServices.Delete("1");

            // Assert
            _deleteUserHandlerMock.Verify(handler => handler.Handle(It.IsAny<DeleteUserCommand>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(true, result);
        }

        [Test]
        public async Task CallGetAllUsersQueryHandler()
        {
            // Arrange
            var expectedUsers = new List<User>() { new User() { Username = "User 1" }, new User() { Username = "User 2" } };
            _getAllUsersHandlerMock.Setup(handler => handler.Handle(It.IsAny<GetAllUsersQuery>(), CancellationToken.None)).ReturnsAsync(expectedUsers);

            // Act
            var result = await _userServices.List();

            // Assert
            _getAllUsersHandlerMock.Verify(handler => handler.Handle(It.IsAny<GetAllUsersQuery>(), CancellationToken.None), Times.Once);
            Assert.AreEqual(expectedUsers, result);
        }
    }
}