﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Saal.DTO
{
    public class User
    {
        public User()
        {
            Roles = new HashSet<Role>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required]
        public string Username { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public virtual ICollection<Role> Roles { get; set; }
    }
}