﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Saal.DTO
{
    public class Role
    {
        public Role()
        {
            Users = new HashSet<User>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleId { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public virtual ICollection<User> Users { get; set; }

    }
}
